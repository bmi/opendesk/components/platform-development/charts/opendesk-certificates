# SPDX-FileCopyrightText: 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0
---
# Helm release cleanup setting.
cleanup:
  # -- Keep certificate on delete of this release.
  keepRessourceOnDelete: true

# The global properties are used to configure multiple charts at once.
global:
  # -- The Top-Level-Domain (TLD) name which is used in f.e. in Ingress component.
  domain: "example.tld"

  # Define the Subdomain for components used in f.e. in Ingress component.
  hosts:
    openxchange: "webmail"

# Issuer references for cert-manager.
issuerRef:
  # -- Name of cert-manager.io Issuer resource.
  name: "letsencrypt-live"
  # -- Type of Issuer, f.e. "Issuer" or "ClusterIssuer".
  kind: "ClusterIssuer"

# -- When true a wildcard certificate will be requested instead of a single certificate with additional names.
wildcard: false

# Create a self-signed certificate
selfSigned:
  # -- Enable self-signed certificate generation.
  enabled: false

  # Root CA certificate settings.
  caCertificate:
    # -- Create a new root CA certificate.
    create: true

    # -- Alternatively specify kubernetes secret which is used as root ca.
    secretName: ""

  # -- Organization used in the self-signed certificate resource.
  organizations:
    - "European Company that Makes Everything (ECME) Inc."

  # -- organizationalUnits used in the self-signed certificate resource.
  organizationalUnits:
    - "Datacenter Operations"

  # Keystore settings.
  keystores:
    # Java keystore settings.
    jks:
      # -- Enable java keystore/truststore creation.
      enabled: true

      # Password for java keystore/truststore.
      password:
        # -- Password as plain value.
        value: ""

        # Password from existing secret (higher precedence than plain value).
        secret:
          # -- Secret name containing password (higher precedence than plain value).
          name: ""

          # -- Key in secret containing the password.
          key: "password"

  # Private key settings
  privateKey:
    # -- Private key algorithm.
    algorithm: "ECDSA"

    # -- Privat key length
    size: 256
...
