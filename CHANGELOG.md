## [3.1.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-certificates/compare/v3.1.0...v3.1.1) (2024-12-11)


### Bug Fixes

* Update reference to Bitname Helm charts to OpenCoDE mirror ([3fd33b9](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-certificates/commit/3fd33b986abead40f069924825874758aac614d8))

# [3.1.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-certificates/compare/v3.0.1...v3.1.0) (2024-06-25)


### Features

* **opendesk-certificates:** Add option for existing root CA kubernetes secret ([fb933b0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-certificates/commit/fb933b003b9fd57d49f669e4bf235e5e1622571e))

## [3.0.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-certificates/compare/v3.0.0...v3.0.1) (2024-06-18)


### Bug Fixes

* **opendesk-certificates:** Update indent of resources and add labels to all resources ([5c2dfc8](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-certificates/commit/5c2dfc86a4adddc809ad24d94b5c9cc391e7d51c))

# [3.0.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-certificates/compare/v2.2.0...v3.0.0) (2024-06-18)


### Features

* **opendesk-certificates:** Add support for self-signed certificates ([064a266](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-certificates/commit/064a266f4592f0d18d84a51d85923371b2dac5f1))


### BREAKING CHANGES

* **opendesk-certificates:** Remove synapse-tls certificate

# [2.2.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-certificates/compare/v2.1.3...v2.2.0) (2024-05-07)


### Features

* **opendesk-certificates:** Add certificate for alternative synapse domain ([a6589f1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-certificates/commit/a6589f16dc845de5fff606f59131c0459f3d8303))

## [2.1.3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-certificates/compare/v2.1.2...v2.1.3) (2024-04-09)


### Bug Fixes

* **opendesk-certificates:** Remove istio certificate ([b366aab](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-certificates/commit/b366aab8c8eb0c635ccad80e473c380a6f3b27c5))

## [2.1.2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-certificates/compare/v2.1.1...v2.1.2) (2023-12-21)


### Bug Fixes

* Add GPG key, update README.md ([ddbe2ae](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-certificates/commit/ddbe2ae46bd8737477cd6b455e98bc16b0e9f62e))

## [2.1.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-certificates/compare/v2.1.0...v2.1.1) (2023-12-20)


### Bug Fixes

* **ci:** Move repo to Open CoDE ([b62c5cb](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-certificates/commit/b62c5cb99b6190d6c99bef9ad4eb04d9d14c58b3))

# [2.1.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-certificates/compare/v2.0.0...v2.1.0) (2023-09-25)


### Features

* **opendesk-certificates:** Only request openxchange domain for istio certificate ([ded1ca5](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-certificates/commit/ded1ca5badfaa0286270a0a560cfac185b70b4f5))

# [2.0.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-certificates/compare/v1.2.2...v2.0.0) (2023-09-18)


### Features

* **opendesk-certificates:** Add wildcard certificate option ([d2392d4](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-certificates/commit/d2392d4f190e060547dbbd41e9eb1015ac3c78af))


### BREAKING CHANGES

* **opendesk-certificates:** Rename sovereign-workplace-certificates to opendesk-certificates

## [1.2.2](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-certificates/compare/v1.2.1...v1.2.2) (2023-08-13)


### Bug Fixes

* **sovereign-workplace-certificates:** Include base domain into certificate ([7737944](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-certificates/commit/77379440e21e3629a6135b4d9762a07265714540))

## [1.2.1](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-certificates/compare/v1.2.0...v1.2.1) (2023-07-19)


### Bug Fixes

* **sovereign-workplace-certificates:** Use isito domain for istio certificate ([d4a075c](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-certificates/commit/d4a075cd6bdaf0c03bd775402b0c22052a8580d4))

# [1.2.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-certificates/compare/v1.1.3...v1.2.0) (2023-07-18)


### Features

* **sovereign-workplace-certificates:** generate certs in istio for all hosts ([cee3bd8](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-certificates/commit/cee3bd8295dce4d45c401239baf9ac2139d066e0))

## [1.1.3](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-certificates/compare/v1.1.2...v1.1.3) (2023-07-17)


### Bug Fixes

* **sovereign-workplace-certificates:** Fix missing node content on disabled istio ([07cd5b1](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-certificates/commit/07cd5b18131d1aa2d850a06de9ac3b8b1fe982fc))

## [1.1.2](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-certificates/compare/v1.1.1...v1.1.2) (2023-07-16)


### Bug Fixes

* Add detailed license information ([89a646b](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-certificates/commit/89a646b5955e1e1aa63c3c75c1c142bb89774f69))

## [1.1.1](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-certificates/compare/v1.1.0...v1.1.1) (2023-07-16)


### Bug Fixes

* **sovereign-workplace-certificates:** Add detailed license information ([d0e0750](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-certificates/commit/d0e0750f016b19078aee88004d325631003f09e5))

# [1.1.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-certificates/compare/v1.0.0...v1.1.0) (2023-07-16)


### Features

* **sovereign-workplace-certificates:** Add additional istio certificate ([fc9eb56](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-certificates/commit/fc9eb5694045c04c3a4c3dd13e2a461a954278af))

# 1.0.0 (2023-06-26)


### Bug Fixes

* **sovereign-workplace-certificates:** Add description to chart ([d0c5494](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-certificates/commit/d0c5494418da0d8c4cec8a45f947b058d00bedc8))


### Features

* **ci:** Add Gitlab CI release process ([0cae8af](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-certificates/commit/0cae8af3b7ac4d00784bd26837a00d731e165bfa))
* Initial release ([8b76d50](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-certificates/commit/8b76d50eab5af7020a94f7b32bfe827135a32765))
<!--
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"

SPDX-License-Identifier: Apache-2.0
-->
